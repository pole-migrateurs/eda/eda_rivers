---
title: "Test for tables"
author: "Cedric"
date: "29 octobre 2019"
output: html_document
---




<table class="table table-striped table-hover table-condensed" style="margin-left: auto; margin-right: auto;">
 <thead>
  <tr>
   <th style="text-align:right;"> no_id </th>
   <th style="text-align:left;"> no_code </th>
   <th style="text-align:left;"> no_type </th>
   <th style="text-align:left;"> no_name </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:right;"> 219 </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:left;"> Obstruction type </td>
   <td style="text-align:left;"> Physical obstruction </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 220 </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:left;"> Obstruction type </td>
   <td style="text-align:left;"> Chemical obstruction </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 218 </td>
   <td style="text-align:left;"> UN </td>
   <td style="text-align:left;"> Obstruction type </td>
   <td style="text-align:left;"> Unknown </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 291 </td>
   <td style="text-align:left;"> DA </td>
   <td style="text-align:left;"> Obstruction_type </td>
   <td style="text-align:left;"> Dam </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 292 </td>
   <td style="text-align:left;"> WE </td>
   <td style="text-align:left;"> Obstruction_type </td>
   <td style="text-align:left;"> Weir </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 293 </td>
   <td style="text-align:left;"> RR </td>
   <td style="text-align:left;"> Obstruction_type </td>
   <td style="text-align:left;"> Rock ramp </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 294 </td>
   <td style="text-align:left;"> CU </td>
   <td style="text-align:left;"> Obstruction_type </td>
   <td style="text-align:left;"> Culvert </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 295 </td>
   <td style="text-align:left;"> FO </td>
   <td style="text-align:left;"> Obstruction_type </td>
   <td style="text-align:left;"> Ford </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 296 </td>
   <td style="text-align:left;"> BR </td>
   <td style="text-align:left;"> Obstruction_type </td>
   <td style="text-align:left;"> Bridge </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 297 </td>
   <td style="text-align:left;"> OT </td>
   <td style="text-align:left;"> Obstruction_type </td>
   <td style="text-align:left;"> Other </td>
  </tr>
</tbody>
</table>
