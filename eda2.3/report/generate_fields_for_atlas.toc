\contentsline {part}{The RN table}{6}{section*.2}
\contentsline {section}{idsegment}{9}{section*.5}
\contentsline {section}{source}{10}{section*.7}
\contentsline {section}{target}{11}{section*.9}
\contentsline {section}{lengthm}{12}{section*.11}
\contentsline {section}{nextdownidsegment}{13}{section*.13}
\contentsline {section}{path}{14}{section*.15}
\contentsline {section}{isfrontier}{15}{section*.17}
\contentsline {section}{issource}{16}{section*.19}
\contentsline {section}{seaidsegment}{17}{section*.21}
\contentsline {section}{issea}{18}{section*.23}
\contentsline {section}{isendoreic}{19}{section*.25}
\contentsline {section}{isinternational}{20}{section*.27}
\contentsline {section}{country}{21}{section*.29}
\contentsline {section}{geom}{22}{section*.31}
\contentsline {part}{The RNA table}{24}{section*.33}
\contentsline {section}{altitudem}{25}{section*.34}
\contentsline {section}{distanceseam}{26}{section*.36}
\contentsline {section}{distancesourcem}{27}{section*.38}
\contentsline {section}{cumnbdam}{28}{section*.40}
\contentsline {section}{cumheightdam}{29}{section*.42}
\contentsline {section}{surfaceunitbvm2}{30}{section*.44}
\contentsline {section}{surfacebvm2}{31}{section*.46}
\contentsline {section}{strahler}{32}{section*.48}
\contentsline {section}{shreeve}{33}{section*.50}
\contentsline {section}{codesea}{34}{section*.52}
\contentsline {section}{name}{35}{section*.54}
\contentsline {section}{pfafriver}{36}{section*.56}
\contentsline {section}{pfafsegment}{37}{section*.58}
\contentsline {section}{basin}{38}{section*.60}
\contentsline {section}{riverwidthm}{39}{section*.62}
\contentsline {section}{riverwidthmsource}{40}{section*.64}
\contentsline {section}{temperature}{41}{section*.66}
\contentsline {section}{wettedsurfacem2}{42}{section*.68}
\contentsline {section}{wettedsurfaceotherm2}{43}{section*.70}
\contentsline {section}{lengthriverm}{44}{section*.72}
\contentsline {section}{emu}{45}{section*.74}
\contentsline {section}{slope}{46}{section*.76}
\contentsline {section}{dis m3 pyr riveratlas}{47}{section*.78}
\contentsline {section}{dis m3 pmn riveratlas}{48}{section*.80}
\contentsline {section}{dis m3 pmx riveratlas}{49}{section*.82}
\contentsline {section}{medianflowm3ps}{50}{section*.84}
\contentsline {section}{drought}{51}{section*.86}
\contentsline {part}{The RNE table: eel predictions}{53}{section*.88}
\contentsline {section}{delta}{54}{section*.89}
\contentsline {section}{gamma}{55}{section*.91}
\contentsline {section}{density}{56}{section*.93}
\contentsline {section}{neel}{57}{section*.95}
\contentsline {section}{beel}{58}{section*.97}
\contentsline {section}{p150}{59}{section*.99}
\contentsline {section}{p150300}{60}{section*.101}
\contentsline {section}{p300450}{61}{section*.103}
\contentsline {section}{p450600}{62}{section*.105}
\contentsline {section}{p600750}{63}{section*.107}
\contentsline {section}{p750}{64}{section*.109}
\contentsline {section}{nsilver}{65}{section*.111}
\contentsline {section}{bsilver}{66}{section*.113}
\contentsline {section}{psilver150300}{67}{section*.115}
\contentsline {section}{psilver300450}{68}{section*.117}
\contentsline {section}{psilver450600}{69}{section*.119}
\contentsline {section}{psilver600750}{70}{section*.121}
\contentsline {section}{psilver750}{71}{section*.123}
\contentsline {section}{psilver}{72}{section*.125}
\contentsline {section}{pmale150300}{73}{section*.127}
\contentsline {section}{pmale300450}{74}{section*.129}
\contentsline {section}{pmale450600}{75}{section*.131}
\contentsline {section}{pfemale300450}{76}{section*.133}
\contentsline {section}{pfemale450600}{77}{section*.135}
\contentsline {section}{pfemale600750}{78}{section*.137}
\contentsline {section}{pfemale750}{79}{section*.139}
\contentsline {section}{pmale}{80}{section*.141}
\contentsline {section}{sex ratio}{81}{section*.143}
\contentsline {section}{cnfemaleXX}{82}{section*.145}
\contentsline {section}{cnmaleXX}{83}{section*.147}
\contentsline {section}{cnsilverXX}{84}{section*.149}
\contentsline {section}{cnsilver}{85}{section*.151}
\contentsline {section}{delta tr}{86}{section*.153}
\contentsline {section}{gamma tr}{87}{section*.155}
\contentsline {section}{density tr}{88}{section*.157}
\contentsline {section}{density pmax tr}{89}{section*.159}
\contentsline {section}{neel pmax tr}{90}{section*.161}
\contentsline {section}{nsilver pmax tr}{91}{section*.163}
\contentsline {section}{sector tr}{92}{section*.165}
\contentsline {section}{neel wd}{93}{section*.167}
\contentsline {section}{beel wd}{94}{section*.169}
\contentsline {section}{nsilver wd}{95}{section*.171}
\contentsline {section}{bsilver wd}{96}{section*.173}
\contentsline {section}{is current distribution area}{97}{section*.175}
\contentsline {section}{is pristine distribution area 1985}{98}{section*.177}
