# EDA2.3

This version works with the sudoang network, in France derived from RHT, in Spain and Portugal derived from WISE network


The sudoang river database can be downloaded here [DOI : 10.5281/zenodo.6384022](https://dx.doi.org/10.5281/zenodo.6384022)

The report is available there [HAL](https://hal.archives-ouvertes.fr/hal-03589288)

The main page of the SUDOANG project is accessible there : [SUDOANG](https://sudoang.eu/en/)
