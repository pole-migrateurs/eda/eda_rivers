# EDA2.1 and 2.2

This version works with RHT in France

It was implemented during the POSE project 2012-2015

> Briand, C., Beaulaton, L., Chapon, P., Drouineau, H., and Lambert, P. 2015. Eel density analysis (EDA 2.2) Estimation de l’échappement en anguilles argentées (Anguilla anguilla) en France. Rapport 2015. ONEMA- EPTB Vilaine, La Roche Bernard.

> Briand, C., Beaulaton, L., Chapon, P., Drouineau, H., and Lambert, P. 2018. Eel density analysis (EDA 2.2.1) Escapement of silver eels (Anguilla anguilla) from French rivers. 2018 report. ONEMA- EPTB Vilaine, La Roche Bernard.
