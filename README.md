# EDA_rivers

Contains code to create and manage the SUDOANG rivers database


This project will be migrated from subversion and TRAC still accessible here : [trac](https://185.135.126.250/traceda)

The sudoang river database can be downloaded here [DOI : 10.5281/zenodo.6384022](https://dx.doi.org/10.5281/zenodo.6384022)

The report is available there [HAL](https://hal.archives-ouvertes.fr/hal-03589288)

The main page of the SUDOANG project is accessible there : [SUDOANG](https://sudoang.eu/en/)

